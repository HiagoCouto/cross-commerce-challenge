# CrossChallenge

## Environment
Docker and docker-compose are used to run the application. Make sure you have them installed.
Also make sure you have ports 80 (nginx) and 5432 (pgsql) available.

After cloning the repository, follow the steps below to run the application:

  * Copy `.env.example` file to `.env` with `cp .env.example .env`
  * Install environment dependencies with `sudo docker-compose build`
  * Run docker containers with `sudo docker-compose up -d`
  * Install project dependencies with `sudo docker-compose exec app composer install`
  * Generate the application key with `sudo docker-compose exec app php artisan key:generate`
  * Change log permissions with `sudo chown -R www-data:www-data storage`

The application will be available on localhost. Visit [`localhost`](http://localhost) from your browser

The process of retrieving pages from the challenge API can take a long time. Therefore, the database of this environment has previously extracted pages. To import, run:

  * `sudo docker-compose exec db bash`
  * `psql --host=localhost --username=postgres --dbname=cross_commerce_challenge < tmp/cross-commerce-challenge.sql`
  * `exit`

However, if you want to get a "clean" database, run the command below:

  * `sudo docker-compose exec app php artisan migrate:fresh`

## Artisan commands:
For more information about the command, accepted parameters and a general description, pass the `--list` option in any of the commands.
  - Extract:
    
    After running `numbers:retrieve` be sure to run `numbers:retry` to recover failed pages. If `numbers:retry` is executed multiple times, the pages will be overwritten in the database according to their id.
 
    - `sudo docker-compose exec app php artisan numbers:retrieve`
    - `sudo docker-compose exec app php artisan numbers:retry`
    
    To see the progress of `numbers:retrieve` run `docker-compose exec app tail -f storage/logs/laravel.log`

  - Transform:
    - `sudo docker-compose exec app php artisan numbers:sort`
    - `sudo docker-compose exec app php artisan numbers:sort --order_by=desc #Sort in descending order`


## Routes:
    - Shuffled numbers
        *  GET  /api/pages              
        *  GET  /api/pages?id=1

    - Sorted numbers (load)
        *  GET  /api/numbers
        *  GET  /api/numbers?page=1

## Tests:
For the tests, run:

    * `sudo docker-compose exec app php artisan test` # or
    * `sudo docker-compose exec app php artisan test --parallel` # To run tests in parallel


