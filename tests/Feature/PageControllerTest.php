<?php

namespace Tests\Feature;

use App\Models\Page;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PageControllerTest extends TestCase
{
    public function test_if_route_exists()
    {
        $response = $this->get('/api/pages');

        $response->assertStatus(200);
    }

    public function test_if_route_not_exists()
    {
        $response = $this->get('/api/page');

        $response->assertStatus(404)->assertJson([
            'error' => 'Not Found',
            'code' => 404
        ]);
    }

    public function test_if_get_pages()
    {
        $id = 1;
        $response = $this->get("/api/pages?id={$id}");

        $response->assertStatus(200)->assertJson([
            'numbers' => json_decode(Page::find($id)?->numbers)
        ]);
    }

    public function test_if_not_get_numbers_by_invalid_id_type()
    {
        $id = 'abc';
        $response = $this->get("/api/pages?id={$id}");

        $response->assertStatus(422)->assertJson([
            'error' => ['id' => ['The id must be an integer.']],
            'code' => 422
        ]);
    }

    public function test_if_not_get_numbers_by_invalid_page_number()
    {
        $id = -1;
        $response = $this->get("/api/pages?id={$id}");

        $response->assertStatus(422)->assertJson([
            'error' => ['id' => ['The id must be at least 1.']],
            'code' => 422
        ]);
    }
}
