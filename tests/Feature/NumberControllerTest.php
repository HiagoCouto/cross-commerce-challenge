<?php

namespace Tests\Feature;

use App\Models\Number;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class NumberControllerTest extends TestCase
{
    public function test_if_route_exists()
    {
        $response = $this->get('/api/numbers');

        $response->assertStatus(200);
    }

    public function test_if_route_not_exists()
    {
        $response = $this->get('/api/number');

        $response->assertStatus(404)->assertJson([
            'error' => 'Not Found',
            'code' => 404
        ]);
    }

    public function test_if_get_numbers()
    {
        $id = 1;
        $response = $this->get("/api/numbers?page={$id}");

        $response->assertStatus(200)->assertJson([
            'numbers' => json_decode(Number::find($id)?->list)
        ]);
    }

    public function test_if_not_get_numbers_by_invalid_page_type()
    {
        $id = 'abc';
        $response = $this->get("/api/numbers?page={$id}");

        $response->assertStatus(422)->assertJson([
            'error' => ['page' => ['The page must be an integer.']],
            'code' => 422
        ]);
    }

    public function test_if_not_get_numbers_by_invalid_page_number()
    {
        $id = -1;
        $response = $this->get("/api/numbers?page={$id}");

        $response->assertStatus(422)->assertJson([
            'error' => ['page' => ['The page must be at least 1.']],
            'code' => 422
        ]);
    }
}
