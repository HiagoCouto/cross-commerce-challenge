<?php

namespace Tests\Unit;

use App\Services\SortNumbers\MergeSort;
use PHPUnit\Framework\TestCase;

class MergeSortTest extends TestCase
{
    public function test_if_sorts_numbers()
    {
        $numbers = range(1, 1000000);
        shuffle($numbers);

        $numbers_to_merge = $numbers;

        MergeSort::sort($numbers_to_merge);
        sort($numbers);

        $this->assertEquals($numbers, $numbers_to_merge);
    }

    public function test_if_sorts_float_numbers()
    {
        $numbers = array_map(fn($num) => $num * (mt_rand()/mt_getrandmax()), range(0.0000001, 0.1, 0.000001));
        shuffle($numbers);

        $numbers_to_merge = $numbers;

        MergeSort::sort($numbers_to_merge);
        sort($numbers);

        $this->assertEquals($numbers, $numbers_to_merge);
    }

    public function test_if_sorts_desc_numbers()
    {
        $numbers = range(1, 100000);
        shuffle($numbers);
        $numbers_to_merge = $numbers;

        MergeSort::sort($numbers_to_merge, false);
        rsort($numbers);

        $this->assertEquals($numbers, $numbers_to_merge);
    }

    public function test_if_sorts_already_sorted_numbers()
    {
        $numbers = range(1, 100000);
        sort($numbers);
        $numbers_to_merge = $numbers;

        MergeSort::sort($numbers_to_merge);

        $this->assertEquals($numbers, $numbers_to_merge);
    }

    public function test_if_sorts_negative_numbers()
    {
        $numbers = range(-1, -100000);
        $numbers_to_merge = $numbers;

        MergeSort::sort($numbers_to_merge);
        sort($numbers);

        $this->assertEquals($numbers, $numbers_to_merge);
    }
}
