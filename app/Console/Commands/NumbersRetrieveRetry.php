<?php

namespace App\Console\Commands;

use App\Models\Page;
use App\Services\GetNumbers;
use Illuminate\Console\Command;

class NumbersRetrieveRetry extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'numbers:retry';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Recovers information from pages marked as faulty in the database.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $page_indexes = Page::where('status', 0)->select('index')->get();
        $failures = $page_indexes->count();

        if (!$failures) {
            $this->info('There are no failures!');
            return 0;
        }

        if ($this->confirm($failures . ' failures. Do you want to try again?', true)) {
            GetNumbers::numbersFromApi($page_indexes->pluck('index')->all());
        }

        return 0;
    }
}
