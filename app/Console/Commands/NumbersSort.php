<?php

namespace App\Console\Commands;

use App\Constants\GetNumbers as ConstantsGetNumbers;
use App\Models\Number;
use App\Models\Page;
use App\Services\SortNumbers\MergeSort;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class NumbersSort extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'numbers:sort {--order_by=asc : Sort in descending (desc) or ascending (asc) order}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Orders all page numbers stored database with merge sort.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $asc = !($this->option('order_by') == 'desc');

        $pages_numbers = Page::where('status', 1)->select('numbers')->get()->pluck('numbers');
        $pages_numbers = $pages_numbers->map(fn ($numbers) => json_decode($numbers))->flatten()->all();
        $pages_numbers = collect(MergeSort::sort($pages_numbers, $asc));
        $count = count($pages_numbers);

        $pages_numbers = $pages_numbers->chunk(ConstantsGetNumbers::POOL_LOT_SIZE)
            ->map(fn ($numbers) => ['list' => $numbers->values()->toJson()])->all();

        DB::table('numbers')->truncate();
        Number::insertOrIgnore($pages_numbers);

        $this->info("$count sorted numbers.");
        return 0;
    }
}
