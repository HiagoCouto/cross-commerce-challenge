<?php

namespace App\Console\Commands;

use App\Models\Page;
use App\Services\GetNumbers;
use Illuminate\Console\Command;
use App\Constants\GetNumbers as ConstantsGetNumbers;

class NumbersRetrieve extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'numbers:retrieve
                            {--indexes= : Comma-separated list of specific page ids}
                            {--limit= : Page limit id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Retrieve the numbers from the API and store in the database.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $options = $this->options();
        $response = GetNumbers::numbersFromApi(...[
            array_filter(explode(',', $options['indexes'])),
            $options['limit']
        ]);

        $request_count = count($response['requests']);
        $message = $request_count * ConstantsGetNumbers::POOL_LOT_SIZE;
        $message .= ' pages recovered in ' . round($response['diff']/60, 2) .  ' minutes.';

        $this->info($message);

        $message_average = 'Average of '.$response['diff']/$request_count.' seconds per batch.';
        $this->info($message_average);

        $message_average = 'Batch size of '.ConstantsGetNumbers::POOL_LOT_SIZE.' requests. ';
        $message_average .= $request_count.' lot(s) in total.';
        $this->info($message_average);

        $page_indexes = Page::where('status', 0)->select('index')->get();
        $failures = $page_indexes->count();

        if (!$failures) {
            return 0;
        }

        if ($this->confirm($failures . ' failures. Do you want to try again?', true)) {
            GetNumbers::numbersFromApi($page_indexes->pluck('index')->all());
        }

        return 0;
    }
}
