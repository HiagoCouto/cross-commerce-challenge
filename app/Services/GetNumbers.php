<?php

namespace App\Services;

use App\Constants\GetNumbers as ConstantsGetNumbers;
use App\Models\Page;
use GuzzleHttp\Exception\ConnectException;
use Illuminate\Http\Client\Pool;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class GetNumbers
{
    public static function numbersFromApi(array $indexes = [], $limit = NULL): array
    {
        $start = time();
        $data = $indexes ? self::getApiNumbersByIndexes($indexes) : self::getApiNumbers($limit);
        $end = time();

        return [
            'requests' => $data['requests'],
            'last_page' => $data['last_uri'],
            'last_response' => $data['last_response'],
            'start' => $start,
            'end' => $end,
            'diff' => ($end - $start)
        ];

    }

    private static function getApiNumbers($limit) {
        $requests_info = [];
        $index = 0;
        $last_uri = '';
        $last_response = NULL;

        do {
            $page = Http::retry(
                ConstantsGetNumbers::RETRIES_NUMBER,
                ConstantsGetNumbers::RETRY_DELAY
            )->get('http://challenge.dienekes.com.br/api/numbers?page=' . $index + 1);

            $response = $page->object();
            if (!($page instanceof ConnectException) && $page->ok() && empty($response->numbers)) break;

            Log::info(strval($index + 1) . '...' . strval($index + ConstantsGetNumbers::POOL_LOT_SIZE));

            $batch = range(($index + 1), $index + ConstantsGetNumbers::POOL_LOT_SIZE);
            $response = self::sendRequests($batch);
            $requests_info[] = $response['request_info'];

            $last_page = $response['last_page'];
            $last_uri = $response['last_uri'];
            $last_response = $response['last_response'];

            $index += ConstantsGetNumbers::POOL_LOT_SIZE;
            if (!empty($limit) && $index >= $limit) break;

        } while (!empty($last_response->numbers) || $last_page instanceof ConnectException || !$last_page->ok());

        return [
            'requests' => $requests_info,
            'last_uri' => $last_uri,
            'last_response' => (array)$last_response
        ];
    }

    private static function getApiNumbersByIndexes($indexes) {
        $requests_info = [];
        $indexes = collect($indexes)->chunk(ConstantsGetNumbers::RETRIES_POOL_LOT_SIZE);
        $chunks_count = $indexes->count();

        foreach ($indexes as $key => $chunk) {
            $index = $key + 1;
            Log::info("Retry chunk {$index} of {$chunks_count}");
            $response = self::sendRequests($chunk);
            $requests_info[] = $response['request_info'];
        }

        return [
            'requests' => $requests_info,
            'last_uri' => $response['last_uri'],
            'last_response' => (array)$response['last_response']
        ];
    }

    private static function sendRequests($batch): array
    {
        $request_info = [];
        $last_uri = '';
        $last_response = NULL;

        $start = time();

        $responses = Http::pool(function (Pool $pool) use ($batch) {
            return collect($batch)
                ->map(fn ($index) => $pool->as($index)->retry(
                    ConstantsGetNumbers::RETRIES_NUMBER,
                    ConstantsGetNumbers::RETRY_DELAY
                )->get("http://challenge.dienekes.com.br/api/numbers?page={$index}"));
        });

        $request_info = [
            'range' => ['start' => $batch[0] ?? 1, 'end' => end($batch)],
            'start' => $start,
            'end' => $end = time(),
            'diff' => ($end - $start)
        ];

        $last_page = end($responses);
        $last_uri = strval(empty($last_page->transferStats) ? $last_uri : $last_page->transferStats->getEffectiveUri());
        $last_response = $last_page instanceof ConnectException ? $last_response : $last_page->object();
        self::storeApiNumbers(collect($responses));

        return [
            'request_info' => $request_info,
            'last_page' => $last_page,
            'last_uri' => $last_uri,
            'last_response' => $last_response
        ];
    }

    private static function storeApiNumbers(Collection $pages): bool
    {
        $pages = $pages->map(function ($page, $index) {
            $failed = $page instanceof ConnectException;

            return [
                'index' => $index,
                'numbers' => $failed ? NULL : json_encode($page?->object()?->numbers ?? []),
                'status' => $failed ? false : $page?->ok() ?? false
            ];
        })->all();

        return Page::upsert($pages, ['index'], ['numbers', 'status']);
    }
}
