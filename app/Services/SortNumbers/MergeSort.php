<?php

namespace App\Services\SortNumbers;

class MergeSort
{
    public static function sort(array &$list, $asc = true)
    {
        $len = count($list);
        if ($len <= 1) {
            return $list;
        }
        $left = array_slice($list, 0, intval($len / 2));
        $right = array_slice($list, intval($len / 2));

        $left = self::sort($left, $asc);
        $right = self::sort($right, $asc);

        self::mergeAsc($list, $left, $right, $asc);
        return $list;
    }

    private static function mergeAsc(&$list, &$left, &$right, $asc)
    {
        $left_head = 0;
        $right_head = 0;
        foreach (array_keys($list) as $index) {
            if (!isset($left[$left_head])) {
                $list[$index] = $right[$right_head];
                $right_head++;
            } elseif (!isset($right[$right_head])) {
                $list[$index] = $left[$left_head];
                $left_head++;
            } elseif ($asc ? $left[$left_head] < $right[$right_head] : $left[$left_head] > $right[$right_head]) {
                $list[$index] = $left[$left_head];
                $left_head++;
            } else {
                $list[$index] = $right[$right_head];
                $right_head++;
            }
        }
    }
}
