<?php

namespace App\Http\Controllers;

use App\Models\Page;
use Illuminate\Http\Request;

class PageController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $data = $request->validate(['id' => 'integer|min:1']);
        $numbers = $request->filled('id') ? Page::where('id', $data['id'])->get() : Page::all();
        $numbers = $numbers->map(fn($number) => json_decode($number->numbers ?? '[]'))->flatten();

        return response()->json(['numbers' => $numbers]) ;
    }
}
