<?php

namespace App\Http\Controllers;

use App\Models\Number;
use Illuminate\Http\Request;

class NumberController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $data = $request->validate(['page' => 'integer|min:1']);
        $numbers = $request->filled('page') ? Number::where('id', $data['page'])->get() : Number::all();
        $numbers = $numbers->map(fn($number) => json_decode($number->list ?? '[]'))->flatten();

        return response()->json(['numbers' => $numbers]) ;
    }
}
