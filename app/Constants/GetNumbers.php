<?php

namespace App\Constants;

class GetNumbers
{
    public const POOL_LOT_SIZE = 100;
    public const RETRIES_POOL_LOT_SIZE = 20;
    public const RETRIES_NUMBER = 5;
    public const RETRY_DELAY = 1000;
}
