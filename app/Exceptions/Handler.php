<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response as HttpFoundationResponse;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array<int, class-string<Throwable>>
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
        });

        $this->renderable(function (Throwable $e) {
            return $this->handleException($e);
        });
    }

    private function handleException(\Throwable $exception)
    {
        $errorParser = fn ($message, $code) => response()->json(['error' => $message, 'code' => $code], $code);

        if ($exception instanceof HttpException) {
            $code = $exception->getStatusCode();
            $message = $exception->getMessage();
            $message = empty($message ) ? HttpFoundationResponse::$statusTexts[$code] : $message;

            return $errorParser($message, $code);
        }

        if($exception instanceof ValidationException) {
            return $errorParser($exception->validator->errors()->getMessages(), Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        return $errorParser('Try later', Response::HTTP_INTERNAL_SERVER_ERROR);
    }
}
